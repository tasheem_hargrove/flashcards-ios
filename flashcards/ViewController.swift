//
//  ViewController.swift
//  flashcards
//
//  Created by Tasheem Hargrove on 1/20/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.darkGray
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width:
                                            80, height: 60))
        label.text = "My Label"
        label.textColor = UIColor.white
        label.font = label.font.withSize(20)
        label.center.x = self.view.center.x
        label.center.y = self.view.center.y
        label.backgroundColor = UIColor.red
        
        self.view.addSubview(label)
    }

}

