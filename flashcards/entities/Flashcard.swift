//
//  Flashcard.swift
//  flashcards
//
//  Created by Tasheem Hargrove on 1/20/22.
//

import Foundation

class Flashcard {
    var term: String
    var definition: String
    
    init(_ term: String, _ definition: String) {
        self.term = term
        self.definition = definition
    }
    
    static func createFlashcards() -> [Flashcard] {
        let flashcards: [Flashcard] = [
            Flashcard("var", "A keyword which signifies a mutable variable in Swift"),
            Flashcard("let", "A keyword which signifies an immutable variable in Swift"),
            Flashcard("init", "Constructor keyword"),
            Flashcard("return", "A keyword used to exit a function/method with a return value"),
            Flashcard("func", "A keyword used to define a function in Swift"),
            Flashcard("->", "A symbol used to specify a return type of a Swift function/method"),
            Flashcard(":", "A symbol used to specify a data type of variable"),
            Flashcard("_", "A sybmol used to allow arguments to be passed to a function without specifying the parameter the argument is associated with"),
            Flashcard("class", "A keyword used to specify a Swift class"),
            Flashcard("struct", "A keyword used to specify a Swift struct")
        ]
        
        return flashcards
    }
}
