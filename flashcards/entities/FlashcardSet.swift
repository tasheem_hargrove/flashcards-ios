//
//  FlashcardSet.swift
//  flashcards
//
//  Created by Tasheem Hargrove on 1/20/22.
//

import Foundation

class FlashcardSet {
    var title: String
    var set: [Flashcard]
    
    init(_ title: String, _ set: [Flashcard]) {
        self.title = title
        self.set = set
    }
    
    func createSet() -> [FlashcardSet] {
        let sets: [FlashcardSet] = [
            FlashcardSet("Set 1", Flashcard.createFlashcards()),
            FlashcardSet("Set 2", Flashcard.createFlashcards()),
            FlashcardSet("Set 3", Flashcard.createFlashcards()),
            FlashcardSet("Set 4", Flashcard.createFlashcards()),
            FlashcardSet("Set 5", Flashcard.createFlashcards()),
            FlashcardSet("Set 6", Flashcard.createFlashcards()),
            FlashcardSet("Set 7", Flashcard.createFlashcards()),
            FlashcardSet("Set 8", Flashcard.createFlashcards()),
            FlashcardSet("Set 9", Flashcard.createFlashcards()),
            FlashcardSet("Set 10", Flashcard.createFlashcards())
        ]
        
        return sets
    }
}
